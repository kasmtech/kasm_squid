Forward Proxy for Seamless Browser
===================================
This solution will redirect the user to an on-premise or cloud based Kasm intallation, launch a Kasm Browser, and navigate to the originally requested site, providing a seamless isolated browsing experience.. This is an explicit proxy and thus requires proxy settings in the user's browser as well as root CA trust.

Install
-------
The install is a simple one line command, the -k option needs to specify the base URL of the Kasm installation.

     sudo ./install_redirector.sh -k https://kasm.company.internal

Client Proxy Settings
---------------------
Clients will need forward proxy settings. You will need the IP address or hostname of Kasm Squid server and the port number, which will be 3122.

###Firefox
Firefox needs to be configured within the Firefox application. Open up Preferences, scroll to the bottom and select Network Settings. The following screen shot shows the settings that need to be confired.

1. Check the manual proxy configuration radial check box
2. Set the HTTP Proxy to the ip address of the squid server you deployed
3. Set the Port to 3122
4. Check the "Use this proxy server for all protocols" checkbox
5. Put in the URL of your Kasm installation in the "No proxy for" text box. In this example it is https://kasm.company.internal

![Firefox Settings](https://kasm-static-content.s3.amazonaws.com/squid_proxy_setup_firefox_settings.png "Firefox Settings")

###Most Windows Browsers
Chrome, IE, and Edge utilize the Operating System proxy settings.

https://www.digitalcitizen.life/how-set-proxy-server-windows-10

Ideally you would push proxy settings out via GPO

https://support.gfi.com/hc/en-us/articles/360012858714-How-to-force-proxy-settings-via-group-policy

Root CA Cert
------------

Each time you install a root CA is installed at the following location, if it doesn't already exist.

     /srv/kasm_squid/ssl_cert

The myCA.der file is to be installed on local client browsers as a trusted certificate authority. The following links provide directions on how to install trusted root CAs in Firefox and Chrome respectively. 

https://wiki.wmtransfer.com/projects/webmoney/wiki/Installing_root_certificate_in_Mozilla_Firefox

Chrome, IE, and Edge will use the Root Certificates managed by Windows.

https://windowsreport.com/install-windows-10-root-certificates/

Ideally you would push out a GPO to target systems, after testing manually.

https://support.securly.com/hc/en-us/articles/206688537-How-to-push-the-Securly-SSL-certificate-with-Active-Directory-GPO-


Standard Forward Proxy
======================

This is containerized, but the install script currently only supports ubuntu. This is just a proxy server, not used for seamless browser isolation.

To use just install it, runs by default on port 3122

     sudo ./install.sh

Automatically starts the container. Container is set to start on boot.
