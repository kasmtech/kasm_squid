#!/bin/bash

function install() {

  if [ -f /etc/os-release ] ; then
    OS_ID="$(awk -F= '/^ID=/{print $2}' /etc/os-release)"
    OS_VERSION_ID="$(awk -F= '/^VERSION_ID/{print $2}' /etc/os-release)"
  fi

  if [ "${OS_ID}" == "ubuntu" ] && ( [ "${OS_VERSION_ID}" == '"16.04"' ] || [ "${OS_VERSION_ID}" == '"18.04"' ] ) ; then
    echo "Ubuntu 16.04/18.04 Install"
    echo "Installing Base Ubuntu Packages"

    # Add a swap file if a swap does not exist. This allows us to install on micro/nano instances with at least some assurance
    if [[ $(sudo swapon --show) ]]; then
      echo 'Swap Exists'
    else
      echo 'Adding Swap'
      fallocate -l 1G /swapfile
      chmod 600 /swapfile
      mkswap /swapfile
      swapon /swapfile
      swapon --show
    fi

    apt-get update
    apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common

    if dpkg -s docker-ce | grep Status: | grep installed ; then
      echo "Docker Installed"
    else
      echo "Installing Docker-CE"

      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      apt-get update
      apt-get -y install docker-ce
    fi

    if [ ! -f /usr/local/bin/docker-compose ]; then
        echo "Installing Docker Compose"
        curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
    fi
  else
    echo "Unsupported OS"
    exit 1
  fi

  mkdir -p /srv/kasm_squid/cache
  mkdir -p /srv/kasm_squid/plugins
  cp ./docker-compose-redirector.yaml /srv/kasm_squid/docker-compose.yaml
  #cp ./squid.redirector.conf /srv/kasm_squid/squid.conf
  cp ./plugins/* /srv/kasm_squid/plugins/

  # cert stuff
  mkdir -p /srv/kasm_squid/ssl_cert
  cd /srv/kasm_squid/ssl_cert
  if [ ! -f /srv/kasm_squid/ssl_cert/myCA.pem ]; then
    openssl req -new -newkey rsa:2048 -sha256 -days 3650 -nodes -x509 -extensions v3_ca -subj "/C=US/ST=CA/O=Kasm Technologies/CN=www.kasmweb.com" -keyout myCA.pem  -out myCA.pem
    openssl x509 -in myCA.pem -outform DER -out myCA.der
  fi

  cd /srv/kasm_squid

  # replace kasm url
  sed -i "s#KASM_URL=\S*#KASM_URL=${INSTALL_KASM_URL}#" /srv/kasm_squid/docker-compose.yaml
  # have squid ignore the kasm cert
  KASM_DOMAIN=$(echo "${INSTALL_KASM_URL}" | sed -r 's#(https://|http://)##')
  sed -i "s#<kasmserver>#${KASM_DOMAIN}#" /srv/kasm_squid/docker-compose.yaml

  # remove any existing container
  set +e
  docker rm -f kasm_squid
  set -e

  docker-compose up -d
}


set -e

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

function display_help() {
  echo "Usage: ${0}"
  echo "-k [URL]  Destination Kasm url. Example https://kasm.company.internal"
  exit 1
}

INSTALL_KASM_URL=""

while getopts ":k:" opt; do
  case ${opt} in
    k) #install proxy
      INSTALL_KASM_URL=$OPTARG
      ;;
    \?)
      display_help
      ;;
    *)
      display_help
      ;;
  esac
done

if [ "${INSTALL_KASM_URL}" == "" ] ;
then
  display_help
  exit 1
else
  install
fi
